# Piero contributors

The following people have contributed to Piero.

-   [Oslandia](http://www.oslandia.com)
    -   [Augustin Trancart](https://github.com/autra)
    -   [Benoît Blanc](https://gitlab.com/benoitblanc)
    -   [Sébastien Guimmara](https://github.com/sguimmara)
    -   [Sylvain Beorchia](https://github.com/sylvainbeo)
    -   [Thomas Muguet](https://github.com/tmuguet)

The following organizations supported Piero:

-   BPI France (<https://www.bpifrance.com/>)
-   Oslandia (<http://www.oslandia.com>)

---

Funded by the French government as part of France 2030.

Funded by the European Union - Next Generation EU as part of the France Relance plan.
